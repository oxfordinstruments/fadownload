import math
import os
from pprint import pprint
import datetime
import requests
from termcolor import colored
from tqdm import tqdm
from faDownload import fad_utils

class FADApi:
    apiKey = None
    session = None

    def __init__(self, _settings):
        self.utils = fad_utils.FADUtils(_settings['debug'])
        self.settings = _settings
        self.debug = _settings['debug']
        self.apiKey = _settings['apiKey']
        self.pageSize = _settings['apiPageSize']
        self.apiUrl = _settings['apiUrl']
        self.utils = fad_utils.FADUtils(self.debug)
        self.setupSession()

    def checkAsset(self, uuid):
        url = "{0}asset/{1}"
        r = self.session.get(url.format(self.apiUrl, uuid))
        if r.status_code != 200:
            return False

        return r.json()

    def getJobs(self, save=False):
        data = {}
        curPage = 0
        startDate = datetime.datetime.now()
        endDate = startDate - datetime.timedelta(days=int(self.settings['days']))
        startDate = startDate.strftime('%Y-%m-%dT%H%%3A%M')
        endDate = endDate.strftime('%Y-%m-%dT%H%%3A%M')

        url = "{0}job/?start={1}&end={2}&pageSize={3}&page={4}&sortedBy=jobId&location={5}"
        r = self.session.get(url.format(self.apiUrl, endDate, startDate, self.pageSize, curPage, self.settings['asset']['location']['uuid']))
        rjs = r.json()
        if self.settings['debugReq']:
            print(colored('Request Output Page 0:', 'magenta'))
            pprint(rjs)
        count = rjs['count']
        pages = int(math.ceil(count/self.pageSize))
        for item in rjs['items']:
            web_link = str(item['link']['url']).replace("https://api.fieldaware.net/job/", "https://app.fieldaware.com/account/jobs_and_quotes/jobs/")
            data[item['jobId']] = {'uuid': item['uuid'], 'api_link':item['link']['url'], 'web_link': web_link}


        pbar = tqdm(iterable=curPage, total=pages)
        while curPage < pages:
            curPage += 1
            r = self.session.get(url.format(self.apiUrl, endDate, startDate, self.pageSize, curPage, self.settings['asset']['location']['uuid']))
            rjs = r.json()
            if self.settings['debugReq']:
                print(colored('Request Output Page {}:'.format(curPage), 'magenta'))
                pprint(rjs)

            for item in rjs['items']:
                web_link = str(item['link']['url']).replace("https://api.fieldaware.net/job/", "https://app.fieldaware.com/account/jobs_and_quotes/jobs/")
                data[item['jobId']] = {'uuid': item['uuid'], 'api_link': item['link']['url'], 'web_link': web_link}

            pbar.update()

        if save:
            self.utils.saveJson(os.path.join(self.settings['base_path'], 'data/jobs.json'), data)

        return data

    def filterJobs(self, save=False):
        wrongs = []
        jobs = self.utils.loadJson(os.path.join(self.settings['base_path'], 'data/jobs.json'))


        for job in tqdm(jobs):
            r = self.session.get(jobs[job]['api_link'])
            rjs = r.json()
            if rjs['asset']['uuid'] != self.settings['asset']['uuid']:
                wrongs.append(job)

        for wrong in wrongs:
            jobs.pop(wrong, None)

        if save:
            self.utils.saveJson(os.path.join(self.settings['base_path'], 'data/jobs.json'), jobs)

    def setupSession(self):
        self.session = requests.Session()
        self.session.headers.update({'Authorization': 'Token {}'.format(self.apiKey)})
        self.session.headers.update({'Accept-Type': 'application/json'})
        if self.debug:
            self.utils.log_str_info("Session Headers: {}".format(str(self.session.headers)))