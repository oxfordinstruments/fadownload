import json
import os
import argparse
from termcolor import colored
import logging
import platform


class FADUtils:
    settings = {}

    def __init__(self, _debug=False):
        self.debug = _debug

    def makeSettingsJson(self):
        j = {
            "apiKey": "",
            "fieldawareUsername": "",
            "fieldawarePassword": "",
            "apiUrl": "https://api.fieldaware.net/",
            "chromeHeadless": False,
            "apiPageSize": 20,
            "delayTime": 2
        }
        print(colored(
            "\n\nSettings.json has been created.\nPlease edit the file and fill in the missing info before continuing.\n\n",
            'magenta'))
        self.saveJson(os.path.join(self.getBasePath(), 'settings.json'), j)

    def loadSettings(self):
        if not os.path.isfile(os.path.normpath(os.path.join(self.getBasePath(), 'settings.json'))):
            print(colored(
                "\n\nError settings.json not found!\nPlease run \'{0} -s\' to create the settings file.\nOnce created edit the file and fill in the missing info.\nThen re-run {0} with the desired flags.\n\n".format(
                    os.path.basename(__package__)), 'red'))
            exit(1)
        with open(os.path.normpath(os.path.join(self.getBasePath(), 'settings.json')), 'r') as fp:
            data = json.load(fp)

        self.settings = data
        self.settings['base_path'] = self.getBasePath()
        self.settings['windows_os'] = self.getIsWindowsOS()
        return self.settings

    def getBasePath(self):
        return os.path.normpath(os.path.abspath(__file__).replace(os.path.basename(__file__), ''))

    def saveJson(self, file, data):
        file = os.path.normpath(file)
        os.makedirs(os.path.dirname('{}'.format(file)), exist_ok=True)

        with open('{}'.format(file), 'wt') as out:
            json.dump(data, out, sort_keys=True, indent=4, separators=(',', ': '))

    def loadJson(self, file):
        file = os.path.normpath(file)
        if not os.path.isfile('{}'.format(file)):
            return False
        with open('{}'.format(file)) as infile:
            data = json.load(infile)
        return data

    def log_str_info(self, string):
        if self.debug:
            logging.info('DEBUG>> ' + str(string))

    def log_str_error(self, string):
        if self.debug:
            logging.error('DEBUG>> ' + str(string))

    def log_raw_info(self, raw):
        if self.debug:
            logging.info(raw)

    def log_raw_error(self, raw):
        if self.debug:
            logging.error(raw)

    def getIsWindowsOS(self):  # Return true if windows os
        if platform.system() != "Windows":
            self.log_str_info("Non-Windows OS Found")
            return False
        else:
            self.log_str_info("Windows OS Found")
            return True