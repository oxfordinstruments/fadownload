import logging
import sys

if sys.version_info<(2,7,0):
  sys.stderr.write("You need python 2.7 or better to run this script\n")
  exit(1)

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())