import json
import os
import time

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import WebDriverException, NoSuchElementException, TimeoutException
from termcolor import colored
import requests
import pprint
from tqdm import tqdm
import numpy
import logging
import platform
from pprint import pprint
from faDownload import fad_utils

class FADWeb:
    faUsername = None
    faPassword = None
    driver = None
    debug = False
    chromedriver_path = ""
    opts = webdriver.ChromeOptions()
    cdr = None
    headless = False
    session = None
    debugRequests = False
    delayTime = None

    def __init__(self, _settings, _cdr=None):
        self.utils = fad_utils.FADUtils(_settings['debug'])
        self.settings = _settings
        self.debug = _settings['debug']
        self.chromedriver_path = os.path.normpath(os.path.join(self.settings['base_path'], "chromedriver.exe"))
        if not self.utils.getIsWindowsOS():
            self.chromedriver_path = os.path.normpath(os.path.join(self.settings['base_path'], "chromedriver"))
        self.utils = fad_utils.FADUtils(self.debug)
        self.faUsername = _settings['fieldawareUsername']
        self.faPassword = _settings['fieldawarePassword']
        self.cdr = _cdr

    def closeDriver(self):
        self.driver.close()

    def startDriver(self):
        if self.debug:
            self.utils.log_str_info(self.chromedriver_path)
        if self.cdr is None:
            prefs = {"download.default_directory": self.settings['assetdir'], "directory_upgrade": "true"}
            self.opts.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0")
            self.opts.add_experimental_option("prefs", prefs)
            if self.headless:
                self.opts.add_argument('--headless')
            self.driver = webdriver.Chrome(chrome_options=self.opts, executable_path=self.chromedriver_path)
            if self.debug:
                self.utils.log_str_info("Driver Url: {}".format(self.driver.command_executor._url))
                self.utils.log_str_info("Driver Session: {}".format(self.driver.session_id))
        else:
            self.driver = webdriver.Remote(command_executor=self.cdr[1], desired_capabilities={})
            self.driver.session_id = self.cdr[0]

    def login(self):
        self.driver.get('https://auth.fieldaware.com/')
        search_box = self.driver.find_element_by_id('email')
        search_box.send_keys(self.faUsername)
        search_box = self.driver.find_element_by_id('password')
        search_box.send_keys(self.faPassword)
        search_box.submit()
        time.sleep(5)  # give the page some time to load

        print(colored("Waiting for login",'magenta'))
        try:
            WebDriverWait(self.driver, 20).until(
                EC.presence_of_element_located((By.XPATH, "//a[@href='/account/jobs_and_quotes']"))
            )
        except NoSuchElementException as e:
            print(e)
            print(colored("Login failed", 'red'))
            return False
        except TimeoutException as t:
            print(t)
            print(colored("Login failed. Check your credentials", 'red'))
            return False

        print(colored("Logged in", 'magenta'))

        return True

    def downloadJob(self, jobid, url):
        self.driver.get(url)
        try:
            WebDriverWait(self.driver, 20).until(
                EC.presence_of_element_located((By.XPATH, "//span[contains(.,'Job {}')]".format(jobid)))
            )
        except NoSuchElementException as e:
            print(e)
            print(colored("Page load failed", 'red'))
            return False
        except TimeoutException as t:
            print(t)
            print(colored("Page load failed by timeout", 'red'))
            return False
        prt = self.driver.find_element_by_css_selector('li.print a')
        prt.click()
        try:
            WebDriverWait(self.driver, 20).until(
                EC.presence_of_element_located((By.XPATH, "//button[contains(.,'Print PDF')]"))
            )
        except NoSuchElementException as e:
            print(e)
            print(colored("Page load failed", 'red'))
            return False
        except TimeoutException as t:
            print(t)
            print(colored("Page load failed by timeout", 'red'))
            print(colored("URL: {}".format(url), 'red'))
            return False
        sela = self.driver.find_element_by_xpath("//span[@class='collapsable-selector']//span")
        time.sleep(2)
        sela.click()

        pdf = self.driver.find_element_by_xpath("//button[contains(.,'Print PDF')]")
        pdf.click()
        paths = WebDriverWait(self.driver, 120, 1).until(self.every_downloads_chrome)

    def every_downloads_chrome(self, driver):
        if not driver.current_url.startswith("chrome://downloads"):
            driver.get("chrome://downloads/")
        return driver.execute_script("""
            var items = downloads.Manager.get().items_;
            if (items.every(e => e.state === "COMPLETE"))
                return items.map(e => e.file_url);
        """)