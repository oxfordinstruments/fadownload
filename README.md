FieldAware Download Jobs
========================
### Python script using Seleniuum WebDriver to control chromedriver downloading service reports from FieldAware.

#### Please see the requirements.txt for the needed python modules.

----
**For the first run execute "run.cmd -s" to create the settings.json file needed for the script. Open your favorite text editor and fill in the missing values such as api key/token, username, and password.**

----

Executing "run.cmd" with no arguments will print the help text.

----