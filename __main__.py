import argparse, os, logging
from pprint import pprint
import six
from time import sleep
from termcolor import colored
from tqdm import tqdm

from faDownload import fad_utils, fad_web, fad_api

debug = False
debugReq = False
### Set cdr to none to start a new webdriver else set to ['<session>', '<url>']
# cdr = ['7bf53096c27d2bba7a7c35d7eeba935f', 'http://127.0.0.1:62383']
cdr = None

parser = argparse.ArgumentParser(description='Field Aware Download Service Reports For Asset')
opts = parser.add_argument_group('Options')
opts.add_argument('-a', help='(REQUIRED) UUID for asset', type=str, dest='asset', required=True)
opts.add_argument('-d', help='Number of days to get jobs (default 365)', action='store', default=365, dest='days')
opts.add_argument('-s', help='Create settings json file', action='store_true', dest='setup')
opts.add_argument('-v', help='Print debugging info (-v level 1, -vv level 2)', action='count', dest='debug')
args = parser.parse_args()
# pprint(args)
# exit(1)
if args.debug is not None:
    if args.debug >= 1:
        logging.info('DEBUG>> Debug Enabled')
        debug = True

    if args.debug >= 2:
        logging.info('DEBUG>> Debug Requests Enabled')
        debugReq = True

utils = fad_utils.FADUtils(debug)

if args.setup:
    logging.info('DEBUG>> Creating Settings')
    utils.makeSettingsJson()
    exit(0)

settings = utils.loadSettings()
settings['debug'] = debug
settings['debugReq'] = debugReq
settings['days'] = args.days
if debug:
    utils.log_raw_info(settings)

api = fad_api.FADApi(settings)

print(colored('Checking the asset', 'magenta'))
asset = api.checkAsset(args.asset)
if not asset:
    utils.log_str_error("Asset does not exist. Check that you typed the uuid correctly. Exiting!")
    print(colored("Asset does not exist. Check that you typed the uuid correctly. Exiting!", "red"))
    exit(1)

settings['asset'] = asset

if debug:
    utils.log_raw_info(settings)

del api
api = fad_api.FADApi(settings)
print(colored('Gathering the jobs for the location', 'magenta'))
sleep(2)
api.getJobs(True)
print("")
print(colored('Filtering jobs for the requested asset', 'magenta'))
sleep(1)
api.filterJobs(True)

assetdir = os.path.normpath(os.path.join(settings['base_path'], 'data/{}'.format(asset['name'])))
print(colored('Creating the asset folder at {}'.format(assetdir), 'magenta'))
os.makedirs(assetdir, exist_ok=True)

settings['assetdir'] = assetdir

web = fad_web.FADWeb(settings, cdr)

print(colored('Starting Chromedriver', 'magenta'))
web.startDriver()
sleep(3)
if cdr is None:
    print(colored('Logging into FieldAware', 'magenta'))
    if not web.login():
        exit(1)

jobs = utils.loadJson(os.path.join(settings['base_path'], 'data/jobs.json'))
print(colored('Downloading reports... please wait.', 'magenta'))
for job in tqdm(jobs):
    web.downloadJob(job, jobs[job]['web_link'])
    try:
        os.rename(os.path.normpath(os.path.join(assetdir, 'fieldaware_job_sheet.pdf')), os.path.normpath(os.path.join(assetdir, 'job_{}.pdf'.format(job))))
    except FileNotFoundError as fnf:
        web.closeDriver()
        print(colored('ERROR: {}'.format(fnf.strerror), 'red'))
        exit(2)

print(colored('Closing browser', 'magenta'))
web.closeDriver()

print(colored('Complete', 'magenta'))
exit(0)

